# character-frequency
Frequency of different Kannada characters

## How to run?
Save the file content in ```kannada-text```
```bash
$ python3 character_frequencies.py
```

#### Note: Tested only Python 3